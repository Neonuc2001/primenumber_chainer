import csv
import math
import sympy
import numpy as np
from ModuleAkiton import Akiton
# 入力層は多ければ多いほど近似する関数は複雑化する（１つだとただの直線）
# ただ入力層は事前にこちらで増やす（学習プログラムの心理的負担を減らす）
print("用意するデータセットの個数を指定してください。")
DataSet_Num = input()
print("生成方法を選択してださい。1=1から順番、2=素数、非素数がそれぞれ同程度の個数")
DataSet_Alg = input()
DataSet = [[0, 0]]
if DataSet_Alg == "1":
    for i in range(1, int(DataSet_Num)):
        DataSet.append([i, sympy.divisor_count(i)])
elif DataSet_Alg == "2":
    Prime_num, Normal_num = [0, 0]
    n = 2
    while True:
        for i in range(n, int(DataSet_Num) + n):

            divisornum = sympy.divisor_count(i)
            #print("Prime_num:" + str(Prime_num))
            #print("Normal_num:" + str(Normal_num))
            if (Prime_num <= Normal_num and int(divisornum) == 2) or (Normal_num <= Prime_num and int(divisornum) != 2):
                if int(divisornum) == 2:
                    Prime_num += 1
                else:
                    Normal_num += 1
                DataSet.append([i, divisornum])
        n = DataSet[len(DataSet) - 1][0]
        if len(DataSet) >= int(DataSet_Num):
            print("Prime_num:" + str(Prime_num))
            print("Normal_num:" + str(Normal_num))
            break

print(DataSet)

print("桁ごとに分離しますか？Yes=1")
var_input = input()
collapse_Dataset = []
#collapse_Dataset = np.array(collapse_Dataset)
Dataset_sample = []
if var_input == "1":
    for n in DataSet:
        n_list = [0 for x in range(
            1,  len(str(DataSet[-1][0])) + 1 - len(str(n[0])))]
        n_list.extend([int(x) for x in str(n[0])])
        n_list.append(n[1])
        #n_list = np.array(n_list)
        collapse_Dataset.append(n_list)

    DataSet = collapse_Dataset
print("Normal")
print(DataSet)
DataSet = np.array(DataSet)
print("np")
print(DataSet)
np.save("data/DataSet.npy", DataSet, allow_pickle=True)
print("npファイルの作成完了")
# print(DataSet)
# with open('data/DataSet.csv', 'w', newline="\r\n") as f:
#    writer = csv.writer(f, lineterminator="\r\n")
#    for i in DataSet:
#        print(i)
#        writer.writerow(i)

# print("CSVファイルの作成を完了")
