import csv
import difflib
import math

import numpy as np
import chainer
from chainer import serializers, Sequential, Chain
import chainer.links as L
import chainer.functions as F
from sklearn.model_selection import train_test_split

# 損失関数の計算
#   損失関数には自乗誤差(MSE)を使用
old_study_level = 0
max_study_level = 0


def cos_similarity(v1, v2):
    return np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))


class MyChain(Chain):

    def __init__(self, val):
        super(MyChain, self).__init__(
            l1=L.Linear(val, 150),
            l2=L.Linear(150, 70),
            l3=L.Linear(70, 40),
            l4=L.Linear(40, 15),
            l5=L.Linear(15, 1)
        )

    def predict(self, x):
        h1 = F.dropout(F.relu(self.l1(x)), ratio=0.2)
        h2 = F.dropout(F.relu(self.l2(h1)), ratio=0.1)
        h3 = F.dropout(F.relu(self.l3(h2)), ratio=0.1)
        h4 = F.dropout(F.relu(self.l4(h3)), ratio=0.1)
        return self.l5(h4)


def forward(x, y, model):
    t = model.predict(x)
    loss = F.mean_squared_error(y, t)
    return loss


class MyChain_Sep(Chain):
    def __init__(self, val):
        super(MyChain_Sep, self).__init__(
            l1=L.Linear(val, 200),
            l2=L.Linear(200, 50),
            l3=L.Linear(50, 1)
        )

    def __call__(self, x):
        h1 = F.relu(self.l1(x))
        h2 = F.relu(self.l2(h1))
        h3 = F.relu(self.l3(h2))
        return h3


# chainer.print_runtime_info()


# with open('data/DataSet.csv', newline="\r\n") as f:
#DataSet = list(csv.reader(f, lineterminator="\r\n"))
#        DataSet = [row for row in csv.reader(f, lineterminator="\r\n")]
# print(DataSet)
DataSet_np = np.load("data/DataSet.npy", allow_pickle=True)
print("Dataの読み込み成功")


x, t = np.hsplit(DataSet_np, [-1])
print(x)
#x = x[:, 0:3].astype(np.int32)
t = t.astype('int')
#x = x.astype('float32')
# for row in x:
# print(type(row))
x = np.array([row.astype("float32") for row in x])
#x = np.array(x)
t = t.astype('float32')
valInput = input("初期変換完了。データを表示するなら1を入力して")
if valInput == "1":
    print(x)
    print(t)
# 全体を訓練データセットと検証データセットを合わせたものとテストデータセットの 2 つに分割
x_train_val, x_test, t_train_val, t_test = train_test_split(
    x, t, test_size=0.3, random_state=0)
print("データセット、分割")


# ネットワークを作成
# 出力値は約数の一つ、入力値はx依存、ここではx[3]を参照


#net = MyChain(len(x[3]))
net = MyChain(len(x[3]))
print("ネットワーク構築")
# 最適化手法を選択、確率的勾配降下法 (SGD) を利用
optimizer = chainer.optimizers.Adam()
optimizer.setup(net)
print("SGD")
max_net = net
# print("最適化法選択（SGD）")
# 学習行程
# chainer.utils.type_check.eval()
while True:
    for i in range(0, 1000):
        loss = forward(x_train_val, t_train_val, net)
        # if i % 100 == 0:
        # print(math.sqrt(loss.data))  # 現状のMSEを表示
        # print(i)
        # print("\n")
        if not loss.data == loss.data:
            print("Unexpected Error, loss made nan")
            exit()
        optimizer.update(forward, x_train_val, t_train_val, net)
        # net.zerogrands()
        #y_train = net(x_train_val)
        #loss = F.softmax_cross_entropy(y_train, t_train_val)
        # loss.backward()
        # if i % 100 == 0:
        #    print(loss.data)  # 現状のMSEを表示
        #    print(i)
        #    print("\n")
        # optimizer.update()
    print("学習終了")
    # 学習結果

    #t_predict = np.round(net.predict(x_test).data)
    t_predict = net.predict(x_test)
    t_predict = np.round(t_predict.data)
    t_trial_predict = net.predict(x_train_val)
    t_trial_predict = np.round(t_trial_predict.data)
    loss = forward(x_test, t_test, net)
    #predict_test_and = set(list(t_predict)) & set(list(t_test))
    #print(str(predict_test_and / t_test))
    # print("x_test")
    # print(x_test)
    # print("t_test")
    # print(t_test)
    # print("t_predict")
    # print(t_predict)
    # print("学習結果")
    two_np = np.full(t_predict.shape, 2)
    if not t_predict.shape == t_test.shape:
        print("!?")
        exit()
    two_np = two_np.astype("float32")
    prime_predict = (t_predict == two_np)
    prime_test = (t_test == two_np)
    # print(two_np)
    #print(prime_predict == prime_train)
    #print(prime_predict == prime_train)
    print(np.sum(prime_predict == prime_test))
    print(np.sum(prime_test == prime_test))
    s = np.sum(t_predict == t_test) / np.sum(t_test == t_test)
    s_trial = np.sum(t_trial_predict == t_train_val) / \
        np.sum(t_train_val == t_train_val)
    s_prime = np.sum(prime_predict == prime_test) / \
        np.sum(prime_test == prime_test)
    if max_study_level <= s:
        max_study_level = s
        max_net = net
        serializers.save_npz("data/mymodel.npz", net)  # npz形式で書き出し
    print("適合率:" + str(s))
    print("素数適合率:" + str(s_prime))
    print("最大適合率:" + str(max_study_level))
    print("損失関数:" + str(loss.data))
    print("訓練適合率:" + str(s_trial))
    if not old_study_level == 0:
        print("学習成果:" + str(s - old_study_level))
    print("\n")
    if s <= 0.0001:
        print("適合率の顕著な低下を検知、終了します")
        exit()
    # print("Continue?,Yes=1")
    #valInput = input()
    # if not valInput == "1":
    #    break
    old_study_level = s
