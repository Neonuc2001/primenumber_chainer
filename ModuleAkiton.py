import math


def Akiton(n):

    if n == 2 or n == 3:

        return 1

    if math.sqrt(n) == math.floor(math.sqrt(n)):

        return 0

    AnswerCounter = -1

    m = n % 12

    if m == 1 or m == 5:

        for Anum in range(1, math.ceil(math.sqrt(n) / 2)):
            if math.sqrt(n - Anum * Anum * 4) == math.floor(math.sqrt(n - Anum * Anum * 4)):

                AnswerCounter = AnswerCounter * -1

    elif m == 7:

        for Bnum in range(1, math.ceil(math.sqrt(n) / math.sqrt(3))):

            if math.sqrt(n - Bnum * Bnum * 3) == math.floor(math.sqrt(n - Bnum * Bnum * 3)):

                AnswerCounter = AnswerCounter * -1

    elif m == 11:

        for Cnum in range(math.ceil(math.sqrt(n / 3)), math.ceil(math.sqrt(n / 2))):

            if math.sqrt(Cnum * Cnum * 3 - n) == math.floor(math.sqrt(Cnum * Cnum * 3 - n)):

                AnswerCounter = AnswerCounter * -1

    else:

        return 0

    if AnswerCounter == 1:

        return 1

    else:

        return 0
