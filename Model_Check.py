import math
import sympy
import matplotlib.pyplot as plt
import numpy as np
import chainer
from chainer import serializers, Sequential, Chain
import chainer.links as L
import chainer.functions as F


class MyChain(Chain):

    def __init__(self, val):
        super(MyChain, self).__init__(
            l1=L.Linear(val, 150),
            l2=L.Linear(150, 70),
            l3=L.Linear(70, 40),
            l4=L.Linear(40, 15),
            l5=L.Linear(15, 1)
        )

    def predict(self, x):
        h1 = F.dropout(F.relu(self.l1(x)), ratio=0.2)
        h2 = F.dropout(F.relu(self.l2(h1)), ratio=0.1)
        h3 = F.dropout(F.relu(self.l3(h2)), ratio=0.1)
        h4 = F.dropout(F.relu(self.l4(h3)), ratio=0.1)
        return self.l5(h4)


DataSet_np = np.load("data/DataSet.npy", allow_pickle=True)
x, t = np.hsplit(DataSet_np, [-1])
x = np.array([row.astype("float32") for row in x])
DataSet = []
net = MyChain(len(x[3]))
serializers.load_npz("Data/mymodel.npz", net)
chainer.using_config("train",false)
x = []
x_true = []
if False:
    while True:
        x = []
        x_true = []
        print("全体用")
        x_whole = input()

        print("１桁ずつ（大きい方から）")
        for i in range(0, 4):
            val = input()
            x.append(int(val))
        x_true.append(x)
        x = np.array(x_true)
        x = x.astype("float32")
        print(x)
        t = net.predict(x)
        t = np.round(t.data)
        print(t)

        if (sympy.divisor_count(int(x_whole)) == 2 and t == 2) or not(sympy.divisor_count(int(x_whole)) == 2 or t == 2):
            print("Correct prediction")
        else:
            print("Wrong prediction")
        x = x.tolist()
else:
    collapse_Dataset = []
    print("グラフ描画開始")
    for i in range(2, 999999):
        DataSet.append([i])
    DataSet_whole = DataSet
    for n in DataSet:
        n_list = [0 for x in range(
            1,  len(str(DataSet[-1][0])) + 1 - len(str(n[0])))]
        n_list.extend([int(x) for x in str(n[0])])
        # n_list.append(n[1])
        #n_list = np.array(n_list)
        collapse_Dataset.append(n_list)

    DataSet = collapse_Dataset
    DataSet = np.array(DataSet)
    DataSet_whole = np.array(DataSet_whole)
    DataSet = DataSet.astype("float32")
    print(DataSet)
    print("x軸データ回収完了")
    t = net.predict(DataSet)
    print("予測完了")
    t = t.data
    t = t.round()
    print(t)
    # input()
    t = t.astype("int32")
    DataSet_whole = DataSet_whole.astype("int32")
    print("表示します")
    plt.plot(DataSet_whole, t, color="red")
    DataSet_concatenate = np.concatenate([DataSet_whole, t], 1)
    i = 0
    i_prime = 0
    p_prime = 0
    p = 0
    i_list = []
    i_prime_list = []
    p_list = []
    p_prime_list = []
    for n in DataSet_concatenate:
        if sympy.divisor_count(int(n[0])) == 2:
            if n[1] == 2:
                i_prime = i_prime + 1
                i = i + 1
            p_prime = p_prime + 1
            i_prime_list.append(i_prime * 100 / p_prime)
            p_prime_list.append(p_prime)
        if not(sympy.divisor_count(int(n[0])) == 2 or n[1] == 2):
            i = i + 1
        p = p + 1
        i_list.append(i * 100 / p)
        p_list.append(p)
    plt.plot(p_list, i_list, color="blue")
    plt.show()
    print("素数のみグラフを表示")
    plt.plot(p_prime_list, i_prime_list)
    plt.show()
